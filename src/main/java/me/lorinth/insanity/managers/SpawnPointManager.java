package me.lorinth.insanity.managers;

import me.lorinth.insanity.objects.SpawnPointScaling;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;

public class SpawnPointManager {

    private HashMap<String, SpawnPointScaling> worldScaling = new HashMap<>();

    public SpawnPointManager(FileConfiguration config){
        load(config);
    }

    public void load(FileConfiguration config){
        for(String worldName : config.getConfigurationSection("SpawnPointScaling").getKeys(false)){
            if(worldName.equalsIgnoreCase("Enabled")){
                continue;
            }

            worldScaling.put(worldName, new SpawnPointScaling(config, "SpawnPointScaling", worldName));
        }
    }

    public SpawnPointScaling getSpawnPointScaling(String worldName){
        return worldScaling.get(worldName);
    }

}
