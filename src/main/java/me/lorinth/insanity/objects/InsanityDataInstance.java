package me.lorinth.insanity.objects;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;

public class InsanityDataInstance {

    private long startTime;
    private long lastStep;
    private HashMap<String, Integer> currentSpawnPointLevels;
    private double currentDamageModifier;
    private double currentMobSpawnRadius;
    private int currentWorldBorderRadius;
    private long lastPurge;

    String path = "Instance";

    public InsanityDataInstance(FileConfiguration config){
        load(config);
    }

    private void load(FileConfiguration config){
        if(!config.getConfigurationSection("").getKeys(false).contains("Instance")){
            create(config);
            return;
        }


    }

    private void create(FileConfiguration config){
        startTime = System.currentTimeMillis();
        lastStep = System.currentTimeMillis();

        currentSpawnPointLevels = new HashMap<>();
        currentDamageModifier = 1.0;
        currentMobSpawnRadius = 30;
        currentWorldBorderRadius = 2000;
        lastPurge = System.currentTimeMillis();
    }

    public void save(FileConfiguration config){
        con
    }

    public double getCurrentDamageModifier() {
        return currentDamageModifier;
    }

    public double getCurrentMobSpawnRadius() {
        return currentMobSpawnRadius;
    }

    public HashMap<String, Integer> getCurrentSpawnPointLevels() {
        return currentSpawnPointLevels;
    }

    public int getCurrentWorldBorderRadius() {
        return currentWorldBorderRadius;
    }

    public long getLastPurge() {
        return lastPurge;
    }

    public long getLastStep() {
        return lastStep;
    }

    public long getStartTime() {
        return startTime;
    }
    p
}
