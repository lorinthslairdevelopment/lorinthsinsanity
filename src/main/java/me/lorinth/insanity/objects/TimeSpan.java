package me.lorinth.insanity.objects;

import org.bukkit.configuration.file.FileConfiguration;

public class TimeSpan {

    private int days = 0;
    private int hours = 0;
    private int minutes = 0;
    private int seconds = 0;

    public TimeSpan(FileConfiguration config, String path){
        days = getValue(config, path + ".Days");
        hours = getValue(config, path + ".Hours");
        minutes = getValue(config, path + ".Minutes");
        seconds = getValue(config, path + ".Seconds");
    }

    private int getValue(FileConfiguration config, String path){
        Object configValue = config.getString(path);

        if(configValue != null){
            return (int) configValue;
        }
        return 0;
    }

    public int getDays(){
        return days;
    }

    public int getHours(){
        return hours;
    }

    public int getMinutes(){
        return minutes;
    }

    public int getSeconds(){
        return seconds;
    }

}
