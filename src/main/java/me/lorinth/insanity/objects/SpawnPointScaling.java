package me.lorinth.insanity.objects;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

public class SpawnPointScaling {

    private boolean enabled;
    private int levelStepIncrease;
    private int maxLevel;
    private boolean autoPlace;
    private Location spawnPointLocation;

    public SpawnPointScaling(FileConfiguration config, String path, String worldName){
        load(config, path, worldName);
    }

    private void load(FileConfiguration config, String path, String worldName){
        enabled = config.getBoolean(path + ".Enabled");
        levelStepIncrease = config.getInt(path + ".LevelStepIncrease");
        maxLevel = config.getInt(path + ".MaxLevel");
        autoPlace = config.getBoolean(path + ".AutoPlace");
        if(!autoPlace){
            spawnPointLocation = new Location(
                    Bukkit.getWorld(worldName),
                    config.getDouble(path + ".SpawnPointLocation.X"),
                    config.getDouble(path + ".SpawnPointLocation.Y"),
                    config.getDouble(path + ".SpawnPointLocation.Z")
            );
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    public int getLevelStepIncrease() {
        return levelStepIncrease;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public boolean isAutoPlace() {
        return autoPlace;
    }

    public Location getSpawnPointLocation(){
        return spawnPointLocation;
    }
}
