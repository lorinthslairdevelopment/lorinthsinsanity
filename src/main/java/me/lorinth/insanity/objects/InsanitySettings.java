package me.lorinth.insanity.objects;

import org.bukkit.configuration.file.FileConfiguration;

public class InsanitySettings {

    public static TimeSpan StepInterval;
    public static boolean SpawnPointScalingEnabled = false;
    public static boolean DamageModifierScalingEnabled = false;
    public static boolean PlayerRelativeMobSpawningEnabled = false;
    public static boolean WorldBorderEnabled = false;
    public static boolean PurgeEnabled = false;

    public InsanitySettings(FileConfiguration config){
        StepInterval = new TimeSpan(config, "DifficultyStepRate");
        SpawnPointScalingEnabled = config.getBoolean("SpawnPointScaling.Enabled");
        DamageModifierScalingEnabled = config.getBoolean("DamageModifierScaling.Enabled");
        PlayerRelativeMobSpawningEnabled = config.getBoolean("PlayerRelativeMobSpawning.Enabled");
        WorldBorderEnabled = config.getBoolean("WorldBorder.Enabled");
        PurgeEnabled = config.getBoolean("Purge.Enabled");
    }

}
