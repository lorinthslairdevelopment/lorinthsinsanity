package me.lorinth.insanity;

import me.lorinth.insanity.managers.SpawnPointManager;
import me.lorinth.insanity.objects.InsanitySettings;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class InsanityPlugin extends JavaPlugin {

    private static ConsoleCommandSender consoleCommandSender;
    private static InsanitySettings insanitySettings;
    private static SpawnPointManager spawnPointManager;

    @Override
    public void onEnable() {
        ConsoleCommandSender consoleCommandSender = getConsole();
        this.saveDefaultConfig();

        File file = new File(getDataFolder(), "config.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        insanitySettings = new InsanitySettings(config);
        spawnPointManager = new SpawnPointManager(config);


        //Start Time - 7:09
        //every 20 minutes, increase level by 1

        //global damage modifier 1.0
        //every 20 minutes, increase by 0.05

        //At night spawn mobs at a radius around the player
        //decrease radius over time

        //World Border
        //Start at a radius from the world spawn
        //Start at a radius and decrease over time
        //Possible minimum

        //Enable PvP for short time frames
        //5 minutes every hour = purge



        consoleCommandSender.sendMessage("[Insanity]: Plugin was enabled");
    }

    private ConsoleCommandSender getConsole(){
        if(consoleCommandSender == null){
            consoleCommandSender = Bukkit.getConsoleSender();
        }

        return consoleCommandSender;
    }
}
